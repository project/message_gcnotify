<?php

namespace Drupal\Tests\message_gcnotify\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests.
 *
 * @group MessageGcNotify
 */
class MessageGcNotifyFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'message_gcnotify',
  ];

  /**
   * Tests.
   */
  public function test(): void {
    $users['module admin'] = $this->createUser(['administer message gcnotify'], 'administer message gcnotify');

    // Anonymous user has no access to settings page.
    $this->drupalGet('admin/config/message/message-gcnotify');
    $this->assertSession()->statusCodeEquals(403);

    // Module admin user has access to settings page.
    $this->drupalLogin($users['module admin']);
    $this->drupalGet('admin/config/message/message-gcnotify');
    $this->assertSession()->statusCodeEquals(200);
  }

}
