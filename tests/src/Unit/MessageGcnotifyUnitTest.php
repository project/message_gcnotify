<?php

namespace Drupal\Tests\message_gcnotify\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\message_gcnotify\Service\GcNotifyApiService;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests.
 *
 * Test GcNotifyApiService. Separate tests are needed for the exceptions to
 * ensure a clear relationship between expectException*() and the test.
 *
 * @group MessageGcnotify
 */
class MessageGcnotifyUnitTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Set mock config.
    $config_map = [
      'message_gcnotify.settings' => [
        'limit_recipients_in_request' => GcNotifyApiService::DEFAULT_LIMIT_RECIPIENTS_IN_REQUEST,
      ],
    ];
    $config_factory = $this->getConfigFactoryStub($config_map);
    $container = new ContainerBuilder();
    $container->set('config.factory', $config_factory);
    \Drupal::setContainer($container);
  }

  /**
   * Test that method returns NULL when no email addresses provided.
   */
  public function testSendMessage(): void {
    $actual = GcNotifyApiService::sendMessage([], '', '');
    $this->assertNull($actual);
  }

  /**
   * Test.
   */
  public function testBuildBulkRequestNoRows(): void {
    $name = $this->randomMachineName();
    $template_id = $this->randomMachineName();
    $rows = [];

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('No rows of data.');
    GcNotifyApiService::buildBulkRequest($rows, [], $name, $template_id);
  }

  /**
   * Test.
   */
  public function testBuildBulkRequestTooManyRows(): void {
    $name = $this->randomMachineName();
    $template_id = $this->randomMachineName();
    $rows = [];
    $rows = range(1, GcNotifyApiService::DEFAULT_LIMIT_RECIPIENTS_IN_REQUEST + 1);

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Too many rows of data.');
    GcNotifyApiService::buildBulkRequest($rows, [], $name, $template_id);
  }

  /**
   * Test.
   */
  public function testBuildBulkRequestNoEmail(): void {
    $name = $this->randomMachineName();
    $template_id = $this->randomMachineName();
    $rows = [];
    $rows = [
      ['one' => 'two'],
    ];

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('The first column in the $rows must be the email address.');
    GcNotifyApiService::buildBulkRequest($rows, [], $name, $template_id);
  }

  /**
   * Test.
   */
  public function testBuildBulkRequest(): void {
    $name = $this->randomMachineName();
    $template_id = $this->randomMachineName();
    $rows = [
      [
        'email address' => $this->randomMachineName(),
        'key_1' => $this->randomMachineName(),
        'key_2' => $this->randomMachineName(),
      ],
      [
        'key_2' => $this->randomMachineName(),
        'key_1' => $this->randomMachineName(),
        'email address' => $this->randomMachineName(),
      ],
    ];
    $arguments = [];

    $expected = [
      'name' => $name,
      'template_id' => $template_id,
      'rows' => [
        [
          'email address',
          'key_1',
          'key_2',
        ],
        [
          $rows[0]['email address'],
          $rows[0]['key_1'],
          $rows[0]['key_2'],
        ],
        [
          $rows[1]['email address'],
          $rows[1]['key_1'],
          $rows[1]['key_2'],
        ],
      ],
    ];

    $actual = GcNotifyApiService::buildBulkRequest($rows, $arguments, $name, $template_id);
    $this->assertSame($expected, $actual);
  }

}
