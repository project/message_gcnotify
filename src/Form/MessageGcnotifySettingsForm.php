<?php

namespace Drupal\message_gcnotify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\message_gcnotify\Service\GcNotifyApiService;

/**
 * Configure example settings for this site.
 */
class MessageGcnotifySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'message_gcnotify_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'message_gcnotify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('message_gcnotify.settings');

    $form['enabled'] = [
      '#type' => 'radios',
      '#title' => $this->t('Message sending status'),
      '#options' => ['Disabled', 'Enabled'],
      '#default_value' => (int) $config->get('enabled'),
      '#required' => TRUE,
    ];

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Test mode will not send messages, Team mode will send email to a limited number of addresses.'),
      '#options' => [$this->t('Test'), $this->t('Team'), $this->t('Production')],
      '#default_value' => $config->get('mode'),
      '#required' => TRUE,
    ];

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging mode'),
      '#description' => $this->t('Enables verbose logging of activities for debugging purposes. <strong>This should not be enabled in a production environment.</strong>'),
      '#default_value' => $config->get('debug_mode'),
    ];

    $form['baseurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GC Notify base url'),
      '#default_value' => $config->get('baseurl'),
      '#required' => TRUE,
    ];

    $form['limit_recipients_in_request'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address count limit per request'),
      '#description' => $this->t('The maximum number of email addresses to include in a request to GC Notify. GC Notify has a hard limit of 50,000 but, depending on your message size, it may throw a 413 Payload Too Large error on a request under that maximum.'),
      '#default_value' => $config->get('limit_recipients_in_request') ?? GcNotifyApiService::DEFAULT_LIMIT_RECIPIENTS_IN_REQUEST,
      '#attributes' => [
        'type' => 'number',
      ],
      '#required' => TRUE,
      '#maxlength' => 5,
    ];

    $form['prod_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production API Key'),
      '#description' => $this->t('That secret key forms a part of your API key, which follows the format {key_name}-{iss-uuid}-{secret-key-uuid}.'),
      '#default_value' => $config->get('prod_apikey'),
      '#required' => TRUE,
    ];

    $form['prod_template'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production template ID'),
      '#default_value' => $config->get('prod_template'),
      '#required' => TRUE,
    ];

    $form['test_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Testing API Key'),
      '#description' => $this->t('That secret key forms a part of your API key, which follows the format {key_name}-{iss-uuid}-{secret-key-uuid}.'),
      '#default_value' => $config->get('test_apikey'),
      '#required' => TRUE,
    ];

    $form['team_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Team API Key'),
      '#description' => $this->t('That secret key forms a part of your API key, which follows the format {key_name}-{iss-uuid}-{secret-key-uuid}.'),
      '#default_value' => $config->get('team_apikey'),
      '#required' => TRUE,
    ];

    $form['test_template'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Testing template ID'),
      '#default_value' => $config->get('test_template'),
      '#required' => TRUE,
    ];

    $form['test_recipients'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test message recipients'),
      '#description' => $this->t('The email addresses to use with "Send test message". Separate multiple addresses with a comma. Test messages will be sent using the configuration as previously saved.'),
    ];

    $form['actions']['send_test'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send test message'),
      '#submit' => ['::sendTest'],
      '#weight' => 50,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Ensure limit_recipients_in_request is within allowed range.
    $limit = (int) $form_state->getValue('limit_recipients_in_request');
    if ($limit < 1 || $limit > GcNotifyApiService::API_LIMIT_RECIPIENTS_IN_REQUEST) {
      $form_state->setErrorByName('limit_recipients_in_request', $this->t('Address count limit per request be must a positive integer not grater than 50,000.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('message_gcnotify.settings')
      ->set('enabled', $values['enabled'])
      ->set('mode', $values['mode'])
      ->set('debug_mode', $values['debug_mode'])
      ->set('limit_recipients_in_request', (int) $values['limit_recipients_in_request'])
      ->set('baseurl', $values['baseurl'])
      ->set('prod_apikey', $values['prod_apikey'])
      ->set('prod_template', $values['prod_template'])
      ->set('test_apikey', $values['test_apikey'])
      ->set('team_apikey', $values['team_apikey'])
      ->set('test_template', $values['test_template'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Form submission handler for testing.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function sendTest(array &$form, FormStateInterface $form_state): void {
    // Get recipients as array of non-empty values.
    $test_recipients = (string) $form_state->getValue('test_recipients');
    $test_recipients = explode(',', $test_recipients);
    $test_recipients = array_map('trim', $test_recipients);
    $test_recipients = array_filter($test_recipients);

    // Error if no recipients.
    if (!$test_recipients) {
      $this->messenger()->addError($this->t('To send a test message, enter an email address in "Test message recipients".'));
      return;
    }

    // Set message subject and body.
    $subject = 'GC Notify Test ' . date('Y-m-d H:i:s');
    $body = 'Test message body.';

    // Send message, display and log success or error.
    $success = GcNotifyApiService::sendMessage($test_recipients, $subject, $body);
    if ($success === TRUE) {
      $message = $subject . ' sent; success response received.';
      $this->messenger()->addStatus($message);
    }
    else {
      $message = $subject . ' sent; error response received; see logs.';
      $this->messenger()->addWarning($message);
    }
    $this->logger('message_gcnotify')->notice($message);
  }

}
