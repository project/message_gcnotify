<?php

namespace Drupal\message_gcnotify\Plugin\Notifier;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\message\MessageInterface;
use Drupal\message_gcnotify\Service\GcNotifyApiService;
use Drupal\message_notify\Exception\MessageNotifyException;
use Drupal\message_notify\Plugin\Notifier\MessageNotifierBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GC Notify notifier.
 *
 * @Notifier(
 *   id = "gcnotify",
 *   title = @Translation("GC Notify"),
 *   description = @Translation("Send messages via GC Notify"),
 *   viewModes = {
 *     "mail_subject",
 *     "mail_body"
 *   }
 * )
 */
class GcNotify extends MessageNotifierBase {

  /**
   * Whether to try switching to recipient user before rendering.
   *
   * @var bool
   */
  protected $trySwitch = TRUE;

  /**
   * Constructs the email notifier plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The message_notify logger channel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Render\RendererInterface $render
   *   The rendering service.
   * @param \Drupal\message\MessageInterface|null $message
   *   (optional) The message entity. This is required when sending or
   *   delivering a notification. If not passed to the constructor, use
   *   ::setMessage().
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelInterface $logger, EntityTypeManagerInterface $entity_type_manager, RendererInterface $render, ?MessageInterface $message = NULL) {
    // Set configuration defaults.
    $configuration += [
      'mail' => FALSE,
      'language override' => FALSE,
      'from' => FALSE,
    ];

    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $entity_type_manager, $render, $message);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MessageInterface $message = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.message_notify'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $message,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function deliver(array $output = []) {
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->message->getOwner();

    if (!$this->configuration['mail'] && $account->isAnonymous()) {
      // The message has no owner and no mail was passed. This will cause an
      // exception, we just make sure it's a clear one.
      throw new MessageNotifyException('It is not possible to send a Message to an anonymous owner. You may set an owner using ::setOwner() or pass a "mail" to the $options array.');
    }

    $mail = $this->configuration['mail'] ?: $account->getEmail();
    if (!$mail) {
      throw new MessageNotifyException('Email address is missing.');
    }

    $viewModes = [
      'mail_subject',
      'mail_body',
    ];
    foreach ($viewModes as $viewMode) {
      if (!isset($output[$viewMode])) {
        throw new MessageNotifyException('Message is missing ' . $viewMode);
      }
    }

    $subject = PlainTextOutput::renderFromHtml(htmlspecialchars_decode(str_replace("\n", "", $output['mail_subject'])));
    $body = PlainTextOutput::renderFromHtml(htmlspecialchars_decode($output['mail_body']));

    $result = GcNotifyApiService::sendMessage([$mail], $subject, $body);

    return $result;
  }

}
