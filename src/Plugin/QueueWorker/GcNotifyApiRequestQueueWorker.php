<?php

namespace Drupal\message_gcnotify\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\message_gcnotify\Service\GcNotifyApiService;

/**
 * Queue worker for GC Notify API requests delayed due to the daily rate limit.
 *
 * @QueueWorker(
 *   id = "gcnotify_api_request_queue",
 *   title = @Translation("Queue worker for GC Notify API requests delayed due to the daily rate limit."),
 * )
 */
class GcNotifyApiRequestQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $result = GcNotifyApiService::sendHttpRequest($data->request_body);

    switch ($result) {
      case GcNotifyApiService::REQUEST_SUCCESS:
        // Success, do not throw.
        break;

      case GcNotifyApiService::REQUEST_DAILY_LIMIT:
        // Daily limit is hit; should stop processing the queue.
        throw new SuspendQueueException('GC Notify API daily limit exceeded.');

      default:
        throw new \Exception('GC Notify API error; see logs.');
    }
  }

}
