<?php

namespace Drupal\message_gcnotify\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service for processing queues, sending messages to GC Notify.
 */
class ProcessQueues implements ContainerInjectionInterface {

  /**
   * The registered logger for message_gcnotify channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new ProcessQueues object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory object.
   * @param \Drupal\Core\Queue\QueueWorkerManager $queueWorker
   *   The queue worker object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory object.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected QueueFactory $queueFactory,
    protected QueueWorkerManager $queueWorker,
    protected LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->logger = $loggerFactory->get('message_gcnotify');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Process the gcnotify_api_request_queue.
   *
   * Run through the entire queue. Items are only deleted from the queue if they
   * succeed. If there is an error, they are not released, so they cannot be
   * claimed again until the 1 hour lease expires. Since the queue run should
   * not take an hour, an item will not be retried until the following run,
   * which should be the next day.
   */
  public function processGcNotifyApiRequestQueue(): void {
    $debug_mode = $this->configFactory->get('message_gcnotify.settings')->get('debug_mode');

    if ($debug_mode) {
      $this->logger->debug('Starting gcnotify_api_request_queue run.');
    }

    $queue = $this->queueFactory->get('gcnotify_api_request_queue');
    $queueWorker = $this->queueWorker->createInstance('gcnotify_api_request_queue');

    // Run through the entire queue.
    while ($item = $queue->claimItem()) {
      if ($debug_mode) {
        $context = [
          '@item_id' => $item->item_id,
          '@created' => date('c', $item->created),
        ];
        $this->logger->debug('Trying queue item @item_id (created @created).', $context);
      }

      try {
        // Process the item and delete it if it succeeds.
        $queueWorker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        // GC Notify API daily limit exceeded. Log and retry next queue run.
        $context = [
          '%message' => $e->getMessage(),
        ];
        $this->logger->error('SuspendQueueException when running gcnotify_api_request_queue; ending queue run: %message', $context);
        return;
      }
      catch (\Exception $e) {
        // Some other error. Log which item caused the error and continue
        // running the queue. The problem item will be re-tried next queue run.
        $context = [
          '%item_id' => $item->item_id,
          '%message' => $e->getMessage(),
        ];
        // If the problem item is old, raise an error, otherwise a warning.
        $compare_time = new \DateTime('2 days ago');
        if ($item->created < $compare_time->getTimestamp()) {
          $context['%date'] = date('c', $item->created);
          $this->logger->error('Exception when running gcnotify_api_request_queue; error with item %item_id (item created %date): %error', $context);
        }
        else {
          $this->logger->warning('Exception when running gcnotify_api_request_queue; error with item %item_id: %message', $context);
        }
      }
    }

    if ($debug_mode) {
      $this->logger->debug('Ending gcnotify_api_request_queue run.');
    }
  }

}
