<?php

namespace Drupal\message_gcnotify\Service;

/**
 * Service for interacting with the GC Notify API.
 */
class GcNotifyApiService {

  /**
   * The maximum number of recipients allowed per request.
   */
  const API_LIMIT_RECIPIENTS_IN_REQUEST = 50000;

  /**
   * The default for config value limit_recipients_in_request.
   */
  const DEFAULT_LIMIT_RECIPIENTS_IN_REQUEST = 5000;

  /**
   * The API request was a success.
   */
  const REQUEST_SUCCESS = 0;

  /**
   * The API request returned an error.
   */
  const REQUEST_ERROR = 1;

  /**
   * The API request exceeded the daily limit.
   */
  const REQUEST_DAILY_LIMIT = 2;

  /**
   * The API request returned an exception, 500 status.
   */
  const REQUEST_EXCEPTION = 3;

  /**
   * The API request returned a timeout, 504 status.
   */
  const REQUEST_TIMEOUT = 4;

  /**
   * Get config var limit_recipients_in_request limited to the allowed range.
   *
   * @return int
   *   The value to use for limit_recipients_in_request.
   */
  public static function getLimitRecipientsInRequest(): int {
    $limit = (int) \Drupal::config('message_gcnotify.settings')->get('limit_recipients_in_request');

    if ($limit < 1 || $limit > static::API_LIMIT_RECIPIENTS_IN_REQUEST) {
      return static::DEFAULT_LIMIT_RECIPIENTS_IN_REQUEST;
    }

    return $limit;
  }

  /**
   * Send a message to multiple recipients via the GC Notify API.
   *
   * The template is assumed to contain personalization variables of 'subject'
   * and 'body', which will be the same for every email address.
   *
   * If there are more emails than the API limit, it will break them up into
   * multiple API requests.
   *
   * @param array $emails
   *   The email addresses to send the message to.
   * @param string $subject
   *   The subject of the message.
   * @param string $body
   *   The body of the message.
   * @param string[] $arguments
   *   An array of optional arguments. Valid keys: scheduled_for, reply_to_id.
   *
   * @return bool
   *   TRUE if the request returns an HTTP status of 201, FALSE otherwise.
   *   Return NULL if no email addresses are provided.
   */
  public static function sendMessage(array $emails, string $subject, string $body, array $arguments = []): ?bool {
    if (!$emails) {
      return NULL;
    }

    $messages = [];
    foreach ($emails as $email) {
      $messages[] = [
        'email address' => $email,
        'subject' => $subject,
        'body' => $body,
      ];
    }

    return static::sendMultipleMessages($messages, $arguments);
  }

  /**
   * Send messages via the GC Notify API.
   *
   * The template is assumed to contain personalization variables of 'subject'
   * and 'body'.
   *
   * If there are more emails than the API limit, it will break them up into
   * multiple API requests.
   *
   * @param array $messages
   *   An array with keys 'email address', 'subject', and 'body'.
   * @param string[] $arguments
   *   An array of optional arguments. Valid keys: scheduled_for, reply_to_id.
   *
   * @return bool
   *   TRUE if the request returns an HTTP status of 201, FALSE otherwise.
   *   Return NULL if no email addresses are provided.
   */
  public static function sendMultipleMessages(array $messages, array $arguments = []): ?bool {
    if (!$messages) {
      return NULL;
    }

    $debug_mode = \Drupal::config('message_gcnotify.settings')->get('debug_mode');

    // TRUE when all calls to ::sendOrQueueHttpRequest() returned TRUE.
    $return = TRUE;
    // Process messages in chunks no larger than the maximum allowed by the API.
    $message_chunks = array_chunk($messages, static::getLimitRecipientsInRequest());
    if ($debug_mode) {
      $context = [
        '@recipient_count' => count($messages),
        '@chunk_count' => count($message_chunks),
      ];
      \Drupal::logger('message_gcnotify')->debug('Sending message to GC Notify with @recipient_count recipients; using @chunk_count chunks.', $context);
    }
    foreach ($message_chunks as $chunk_key => $message_chunk) {
      $request = static::buildBulkRequest($message_chunk, $arguments);
      $queue_item_id = static::sendOrQueueHttpRequest($request);

      // Log when request added to queue.
      if (gettype($queue_item_id) !== 'boolean') {
        $context = [
          '@chunk_number' => $chunk_key + 1,
          '@chunk_count' => count($message_chunks),
          '@queue_item_id' => $queue_item_id,
        ];
        \Drupal::logger('message_gcnotify')->notice('Chunk @chunk_number of @chunk_count added to queue as item @queue_item_id.', $context);
      }

      // Request did not success, return FALSE from this, do remaining chunks.
      if ($queue_item_id !== TRUE) {
        $return = FALSE;
      }
    }

    return $return;
  }

  /**
   * Returns the template ID to use.
   *
   * In production mode, use the production template, otherwise test template.
   *
   * @return string|null
   *   The template ID or NULL if it is not configured.
   */
  public static function getTemplateId(): ?string {
    $config = \Drupal::config('message_gcnotify.settings');
    return $config->get('mode') == 2 ? $config->get('prod_template') : $config->get('test_template');
  }

  /**
   * Build a request to send using the GC Notify bulk API.
   *
   * @param array[] $rows
   *   An array of rows. The first value in each row must be an email address
   *   and have the key 'email address'. The other values are template
   *   placeholder fields. The keys must be the field name.
   * @param string[] $arguments
   *   An array of optional arguments. Valid keys: scheduled_for, reply_to_id.
   * @param string|null $name
   *   The name of your bulk sending job. When NULL, use default.
   * @param string|null $template_id
   *   The GC Notify template_id. When NULL, use the template ID from config.
   *
   * @return array
   *   The request array.
   *
   * @see https://documentation.notification.canada.ca/en/send.html#sending-notifications-in-bulk
   */
  public static function buildBulkRequest(array $rows, array $arguments = [], ?string $name = NULL, ?string $template_id = NULL): array {
    if (!$rows) {
      throw new \Exception('No rows of data.');
    }

    if (count($rows) > static::getLimitRecipientsInRequest()) {
      throw new \Exception('Too many rows of data.');
    }

    // The data keys, which are assumed to exist in all rows.
    $keys = array_keys($rows[0]);

    // The first key in the first row must be 'email address'.
    if ($keys[0] !== 'email address') {
      throw new \Exception('The first column in the $rows must be the email address.');
    }

    $template_id = $template_id ?: static::getTemplateId();
    if (!$template_id) {
      throw new \Exception('Template ID not configured.');
    }

    $request = [
      'name' => $name ?: 'message_gcnotify',
      'template_id' => $template_id,
      // Use the keys of the first row as the field labels. They should be the
      // same for all rows, but this method does not check for that.
      'rows' => [
        $keys,
      ],
    ];

    // Add each row, removing the keys and normalizing the order to match $keys.
    foreach ($rows as $row) {
      $new_row = [];
      foreach ($keys as $key) {
        $new_row[] = $row[$key] ?? NULL;
      }
      $request['rows'][] = $new_row;
    }

    $optional_arguments = [
      'scheduled_for',
      'reply_to_id',
    ];
    foreach ($optional_arguments as $argument) {
      if (isset($arguments[$argument])) {
        $request[$argument] = $arguments[$argument];
      }
    }

    return $request;
  }

  /**
   * Send an HTTP request to the GC Notify API.
   *
   * @param array $request_body
   *   The data to send as the request body.
   *
   * @return int
   *   One of the constants from this class that starts with "REQUEST_".
   */
  public static function sendHttpRequest(array $request_body): int {
    $config = \Drupal::config('message_gcnotify.settings');
    $endpoint_url = rtrim($config->get('baseurl'), '/') . '/v2/notifications/bulk';

    $debug_mode = $config->get('debug_mode');
    $enabled = $config->get('enabled');

    // Set the key according to the mode.
    $mode = match((int) $config->get('mode')) {
      0 => 'test',
      1 => 'team',
      2 => 'prod',
    };
    $apikey = $config->get($mode . '_apikey');

    $headers = [
      'Content-type' => 'application/json',
      'Authorization' => 'ApiKey-v1 ' . $apikey,
    ];

    $request_body = json_encode($request_body);

    // When enabled and in debug mode, log the API request that will be sent.
    // When not enabled, log API request but do not send it, return success.
    if ($debug_mode || !$enabled) {
      $context = [
        '@mode' => $mode,
        '@request_body' => substr($request_body, 0, 4096),
      ];

      if ($enabled) {
        \Drupal::logger('message_gcnotify')->notice('Body sent to GC Notify (@mode): @request_body', $context);
      }
      else {
        \Drupal::logger('message_gcnotify')->notice('GC Notify disabled; body not sent to GC Notify (@mode): @request_body', $context);
        return static::REQUEST_SUCCESS;
      }
    }

    $options = [
      'headers' => $headers,
      'body' => $request_body,
      'http_errors' => FALSE,
    ];
    $response = \Drupal::httpClient()->post($endpoint_url, $options);

    if ($debug_mode) {
      $context = [
        '@response' => (string) $response->getBody(),
      ];
      \Drupal::logger('message_gcnotify')->notice('GC Notify response: @response', $context);
    }

    // Return an appropriate status code.
    //
    // Success.
    $statusCode = (int) $response->getStatusCode();
    if ($statusCode === 201) {
      return static::REQUEST_SUCCESS;
    }

    // Log the error status.
    $context = [
      '@http_status' => $statusCode,
    ];
    \Drupal::logger('message_gcnotify')->error('GC Notify error @http_status.', $context);

    // The error code to return.
    switch ($statusCode) {
      case 429:
        $return = static::REQUEST_DAILY_LIMIT;
        break;

      case 500:
        $return = static::REQUEST_EXCEPTION;
        break;

      case 504:
        $return = static::REQUEST_TIMEOUT;
        break;

      default:
        $return = static::REQUEST_ERROR;
    }

    // Get the error message.
    $body = json_decode($response->getBody());
    $body->errors ??= [];

    // Errors are provided in an array.
    foreach ($body->errors as $error) {
      $error->error ??= NULL;
      $error->message ??= NULL;

      // Daily limit can have status 400, identifiable only by error text.
      if ($statusCode === 400 && preg_match('/You only have \d+ remaining messages before you reach your daily limit. You\'ve tried to send \d+ messages./', $error->message)) {
        $return = static::REQUEST_DAILY_LIMIT;
      }

      // Log the error.
      $context = [
        '@http_status' => $statusCode,
        '@error' => $error->error,
        '@message' => $error->message,
      ];
      \Drupal::logger('message_gcnotify')->error('GC Notify error @http_status: @error: @message', $context);
    }

    return $return;
  }

  /**
   * Queue an HTTP request to the GC Notify API.
   *
   * @param array $request_body
   *   The data to send as the request body.
   *
   * @return bool|int|string
   *   A unique ID if the item was added to the queue, FALSE otherwise.
   */
  public static function queueHttpRequest(array $request_body): bool|int|string {
    $queue_item = new \stdClass();
    $queue_item->request_body = $request_body;

    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = \Drupal::service('queue');
    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $queue_factory->get('gcnotify_api_request_queue');
    $queue_item_id = $queue->createItem($queue_item);

    if (!$queue_item_id) {
      \Drupal::logger('message_gcnotify')->error('Failed to add item to gcnotify_api_request_queue.',);
    }

    return $queue_item_id;
  }

  /**
   * Attempt to send a request to the GC Notify API.
   *
   * Queue for retry if the daily limit is hit, there was an exception, or the
   * API return a timeout status.
   *
   * @param array $request_body
   *   The data to send as the request body.
   *
   * @return bool
   *   TRUE if the request returns an HTTP status of 201. The queue item
   *   identifier if the item was added to the queue. FALSE otherwise.
   */
  public static function sendOrQueueHttpRequest(array $request_body): bool|int|string {
    $result = static::sendHttpRequest($request_body);

    // Return TRUE for success.
    if ($result === static::REQUEST_SUCCESS) {
      return TRUE;
    }

    // Add to queue and return queue item ID if the daily limit is reached,
    // the API returns 500, or the API returns a timeout status.
    $status_codes = [
      static::REQUEST_DAILY_LIMIT,
      static::REQUEST_EXCEPTION,
      static::REQUEST_TIMEOUT,
    ];
    if (in_array($result, $status_codes, TRUE)) {
      return static::queueHttpRequest($request_body);
    }

    return FALSE;
  }

}
